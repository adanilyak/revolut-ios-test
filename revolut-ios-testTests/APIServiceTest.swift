//
//  APIServiceTest.swift
//  revolut-ios-testTests
//
//  Created by Alexander Danilyak on 03/05/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

@testable import revolut_ios_test

import XCTest

class APIServiceTest: XCTestCase {
    
    var apiServiceConfiguration: APIServiceConfiguration!
    var apiService: APIService!
    
    override func setUp() {
        super.setUp()
        apiServiceConfiguration = APIServiceConfiguration(scheme: "https",
                                                          host: "google.com")
        apiService = APIService(serviceConfiguration: apiServiceConfiguration)
    }
    
    func testCreatePlainURL() {
        let expectedUrl: URL = URL(string: "https://google.com")!
        let createdURL: URL? = apiService.createURL(pathComponents: [])
        XCTAssertEqual(expectedUrl, createdURL, "Plain url creation")
    }
    
    func testCreateParameterizedURL() {
        let expectedUrl: URL = URL(string: "https://google.com?query=abracadabra")!
        let createdURL: URL? = apiService.createURL(pathComponents: [], queryParams: ["query": "abracadabra"])
        XCTAssertEqual(expectedUrl, createdURL, "Parameterized url creation")
    }
    
    func testCreatePathURL() {
        let expectedUrl: URL = URL(string: "https://google.com/search")!
        let createdURL: URL? = apiService.createURL(pathComponents: ["search"])
        XCTAssertEqual(expectedUrl, createdURL, "Path url creation")
    }
    
    func testCreateParameterizedPathURL() {
        let expectedUrl: URL = URL(string: "https://google.com/search?query=abracadabra")!
        let createdURL: URL? = apiService.createURL(pathComponents: ["search"], queryParams: ["query": "abracadabra"])
        XCTAssertEqual(expectedUrl, createdURL, "Path parameterized url creation")
    }
    
}
