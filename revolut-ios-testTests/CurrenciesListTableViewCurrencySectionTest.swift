//
//  CurrenciesListTableViewCurrencySectionTest.swift
//  revolut-ios-testTests
//
//  Created by Alexander Danilyak on 03/05/2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

@testable import revolut_ios_test

import XCTest

class TableViewSectionContraryInteractionProtocolMock: TableViewSectionContraryInteractionProtocol {
    
    func getIndex(of cell: UITableViewCell, for section: TableViewSectionProtocol) -> Int? {
        return 0
    }
    
    func getVisibleCellsIndecies(for section: TableViewSectionProtocol) -> IndexSet? {
        return IndexSet(integer: 0)
    }
    
    func getCell(at index: Int, for section: TableViewSectionProtocol) -> UITableViewCell? {
        return UITableViewCell()
    }
    
    var isInsertCellsCalled: Bool = false
    func insertCells(at indecies: IndexSet, for section: TableViewSectionProtocol) {
        isInsertCellsCalled = true
    }
    
    var isMoveCellCalled: Bool = false
    func moveCell(from oldIndex: Int, to newIndex: Int, and completion: ((Bool) -> Void)?, for section: TableViewSectionProtocol) {
        isMoveCellCalled = true
    }
    
    var isDeselectCalled: Bool = false
    func deselect(at index: Int, animated: Bool, for section: TableViewSectionProtocol) {
        isDeselectCalled = true
    }
    
    var isScrollToCalled: Bool = false
    func scroll(to index: Int, position: UITableViewScrollPosition, for section: TableViewSectionProtocol) {
        isScrollToCalled = true
    }
    
    
}

class CurrenciesListTableViewCurrencySectionProtocolMock: CurrenciesListTableViewCurrencySectionProtocol {
    
    var isCurrenciesListTableViewCurrencySectionBaseCurrencyChangedCalled: Bool = false
    func currenciesListTableViewCurrencySectionBaseCurrencyChanged(to id: CurrencyRate.ID) {
        isCurrenciesListTableViewCurrencySectionBaseCurrencyChangedCalled = true
    }
    
}

class CurrenciesListTableViewCurrencySectionTest: XCTestCase {
    
    var section: CurrenciesListTableViewCurrencySection!
    
    var sectionInteractionDelegate: TableViewSectionContraryInteractionProtocolMock!
    
    var incomingRates: [CurrencyRate] = [CurrencyRate(id: "USD", value: 1.2, base: "EUR"),
                                         CurrencyRate(id: "GBP", value: 0.8, base: "EUR"),
                                         CurrencyRate(id: "RUB", value: 70.0, base: "EUR")]
    
    override func setUp() {
        super.setUp()
        section = CurrenciesListTableViewCurrencySection()
        sectionInteractionDelegate = TableViewSectionContraryInteractionProtocolMock()
        section.interactionDelegate = sectionInteractionDelegate
    }
    
    func testUpdateRates() {
        XCTAssertEqual(section.numberOfRows(), 1, "Only base currency after init")
        section.updateRates(with: incomingRates)
        XCTAssert(sectionInteractionDelegate.isInsertCellsCalled, "Should insert rows")
        XCTAssertEqual(section.numberOfRows(), 4, "Update should add rows")
    }
    
    func testChangeBaseCurrency() {
        section.updateRates(with: incomingRates)
        section.onSelect(at: 2)
        XCTAssert(sectionInteractionDelegate.isDeselectCalled, "Should deselect row")
        XCTAssert(sectionInteractionDelegate.isScrollToCalled, "Should scroll to top row")
        XCTAssert(sectionInteractionDelegate.isMoveCellCalled, "Should move to top row")
        XCTAssertEqual(section.baseCurrencyID, "GBP", "Base should change")
    }
    
}
