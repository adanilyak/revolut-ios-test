//
//  AppDelegate.swift
//  revolut-ios-test
//
//  Created by Alexander Danilyak on 29.04.2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    /// Assembler for building all possible application screens
    private var assembler: ScreensAssembler?
    
    /// Main application router
    private var router: Router?
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        assembler = ScreensAssembler()
        
        guard let assembler: ScreensAssembler = assembler else {
            assert(false, "Assembler must exist")
            return false
        }
        
        router = Router(with: assembler)
        router?.routeToCurrenciesList()
        
        guard let router: Router = router else {
            assert(false, "Router must exist")
            return false
        }
        
        window = createWindow(with: router.rootController)
        window?.makeKeyAndVisible()
        
        return true
        
    }

}

// MARK: - AppDelegate + Windwow
extension AppDelegate {
    
    /// Create main application window
    ///
    /// - Parameter rootController: root controller
    /// - Returns: window
    private func createWindow(with rootController: UIViewController) -> UIWindow {
        let window: UIWindow = UIWindow(frame: UIScreen.main.bounds)
        window.backgroundColor = StyleGuide.shared.backgroundColor
        window.rootViewController = rootController
        return window
    }
    
}

