//
//  Router.swift
//  revolut-ios-test
//
//  Created by Alexander Danilyak on 30.04.2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

/// Main application router
class Router: RouterProtocol {
    
    /// Assembler for building all possible application screens
    let assembler: ScreensAssemblerProtocol
    
    /// Root navigation controller
    let rootController: UINavigationController = UINavigationController()
    
    init(with assembler: ScreensAssemblerProtocol) {
        self.assembler = assembler
    }
    
}

