//
//  RouterProtocol.swift
//  revolut-ios-test
//
//  Created by Alexander Danilyak on 30.04.2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

/// Protocol for incapsulating in-app navigation
protocol RouterProtocol: class {
    
    /// Base root controller
    var rootController: UINavigationController { get }
    
}

