//
//  ScreensAssemblerProtocol.swift
//  revolut-ios-test
//
//  Created by Alexander Danilyak on 30.04.2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

protocol ScreensAssemblerProtocol: class {
    
    func assembleCurrenciesListViewController(with router: CurrenciesListRouter) -> UIViewController
    
}
