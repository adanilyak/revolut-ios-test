//
//  CurrenciesListTableViewCurrencySection.swift
//  revolut-ios-test
//
//  Created by Alexander Danilyak on 03.05.2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

protocol CurrenciesListTableViewCurrencySectionProtocol: class {
    
    func currenciesListTableViewCurrencySectionBaseCurrencyChanged(to id: CurrencyRate.ID)
    
}

class CurrenciesListTableViewCurrencySection {
    
    struct Currency {
        
        var rate: CurrencyRate
        
        var amount: Double
        
    }
    
    //MARK: Data
    
    private var baseCurrency: Currency = Currency(rate: CurrencyRate(id: "EUR", value: 1.0, base: "EUR"),
                                                  amount: 100.0)
    
    var baseCurrencyID: CurrencyRate.ID {
        return baseCurrency.rate.id
    }
    
    private lazy var currencies: [Currency] = [self.baseCurrency]
    
    private var baseCurrencyIndex: Int? {
        return currencies.index(where: { $0.rate.id == baseCurrencyID })
    }
    
    //MARK: Delegates
    
    weak var delegate: CurrenciesListTableViewCurrencySectionProtocol?
    
    weak var interactionDelegate: TableViewSectionContraryInteractionProtocol?
    
}

//MARK: - Update
extension CurrenciesListTableViewCurrencySection {
    
    func updateRates(with incomingRates: [CurrencyRate]) {
        var newRates: [CurrencyRate] = []
        var updatedIndecies: IndexSet = IndexSet()
        
        let currentCurrensiesID: [CurrencyRate.ID] = currencies.map { $0.rate.id }
        for rate in incomingRates {
            if let index: Int = currentCurrensiesID.index(where: { $0 == rate.id }) {
                currencies[index].rate = rate
                currencies[index].amount = rate.value * baseCurrency.amount
                updatedIndecies.insert(index)
            } else {
                newRates.append(rate)
            }
        }
        
        updateVisibleCurrencies()
        
        let insertIndecies: IndexSet = IndexSet(integersIn: currencies.count..<currencies.count + newRates.count)
        currencies.append(contentsOf: newRates.map { Currency(rate: $0, amount: $0.value * baseCurrency.amount) })
        
        interactionDelegate?.insertCells(at: insertIndecies, for: self)
    }
    
    private func updateAmounts() {
        for (index, currency) in currencies.enumerated() where currency.rate.id != baseCurrencyID {
            currencies[index].amount = baseCurrency.amount * currency.rate.value
        }
        
        updateVisibleCurrencies()
    }
    
    private func updateVisibleCurrencies() {
        guard let visibleIndecies: IndexSet = interactionDelegate?.getVisibleCellsIndecies(for: self) else {
            return
        }
        
        visibleIndecies.forEach { index in
            if let currencyCell: CurrencyCell = interactionDelegate?.getCell(at: index, for: self) as? CurrencyCell,
                currencies[index].rate.id != baseCurrencyID {
                currencyCell.configure(with: convertToItem(currencies[index]))
            }
        }
    }
    
    private func convertToItem(_ currency: Currency) -> CurrencyCellItem {
        return CurrencyCellItem(currencyKey: currency.rate.id,
                                value: currency.amount)
    }
    
}

// MARK: - TableViewSectionProtocol
extension CurrenciesListTableViewCurrencySection: TableViewSectionProtocol {
    
    func cellClassesAndReuseIdentifiers() -> [(UITableViewCell.Type, String)] {
        return [(CurrencyCell.self, CurrencyCell.description())]
    }
    
    func numberOfRows() -> Int {
        return currencies.count
    }
    
    func cellReuseIdentifier(for row: Int) -> String {
        return CurrencyCell.description()
    }
    
    func cell(for row: Int, dequeuedCell: UITableViewCell) -> UITableViewCell {
        guard let currencyCell: CurrencyCell = dequeuedCell as? CurrencyCell else {
            assert(false, "[\(self)] Wrong cell type")
            return UITableViewCell()
        }
        
        currencyCell.configure(with: convertToItem(currencies[row]))
        currencyCell.delegate = self
        return currencyCell
    }
    
    func onSelect(at index: Int) {
        interactionDelegate?.deselect(at: index, animated: true, for: self)
        
        let newBaseCurrencyID: CurrencyRate.ID = currencies[index].rate.id
        baseCurrency = Currency(rate: CurrencyRate(id: newBaseCurrencyID,
                                                   value: 1.0,
                                                   base: newBaseCurrencyID),
                                amount: currencies[index].amount)
        
        currencies.remove(at: index)
        currencies.insert(baseCurrency, at: 0)
        
        interactionDelegate?.scroll(to: 0, position: .top, for: self)
        
        let completion: (Bool) -> Void = { [weak self] _ in
            guard let strongSelf = self else { return }
            strongSelf.interactionDelegate?.getCell(at: 0, for: strongSelf)?.becomeFirstResponder()
        }
        
        interactionDelegate?.moveCell(from: index,
                                      to: 0,
                                      and: completion,
                                      for: self)
        
        delegate?.currenciesListTableViewCurrencySectionBaseCurrencyChanged(to: baseCurrencyID)
    }
    
}

// MARK: - CurrencyCellProtocol
extension CurrenciesListTableViewCurrencySection: CurrencyCellProtocol {
    
    func currencyCellInputDidChanged(to input: String, in cell: CurrencyCell) {
        guard let row: Int = interactionDelegate?.getIndex(of: cell, for: self),
            let baseCurrencyIndex = baseCurrencyIndex,
            baseCurrencyIndex == row else {
                return
        }
        
        baseCurrency.amount = currencyDecimalNumberFormatter.number(from: input)?.doubleValue ?? 0.0
        currencies[baseCurrencyIndex] = baseCurrency
        
        updateAmounts()
    }
    
}
