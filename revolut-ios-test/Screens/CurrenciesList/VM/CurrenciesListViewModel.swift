//
//  CurrenciesListViewModel.swift
//  revolut-ios-test
//
//  Created by Alexander Danilyak on 30.04.2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation
import RxSwift

protocol CurrenciesListViewModelProtocol: class {
    
    func vmUpdatePrompt(with baseID: String)
    
}

class CurrenciesListViewModel: AbstactTableViewMediator {
    
    private let router: CurrenciesListRouter
    
    private let api: APIServiceProtocolCurrency
    
    weak var delegate: CurrenciesListViewModelProtocol? {
        didSet {
            delegate?.vmUpdatePrompt(with: currenciesSection.baseCurrencyID)
        }
    }
    
    private var dispose: DisposeBag = DisposeBag()
    
    //MARK: Sections
    
    private lazy var currenciesSection: CurrenciesListTableViewCurrencySection = {
        let section: CurrenciesListTableViewCurrencySection = CurrenciesListTableViewCurrencySection()
        section.interactionDelegate = self
        section.delegate = self
        return section
    }()
    
    override var sections: [TableViewSectionProtocol] {
        return [currenciesSection]
    }
    
    //MARK: Delegate
    
    init(with router: CurrenciesListRouter,
         apiServiceCurrency: APIServiceProtocolCurrency) {
        self.router = router
        self.api = apiServiceCurrency
    }
    
    //MARK: Loading
    
    /// Activate procedure of periodical update of rates
    ///
    /// - Parameter interval: interval of updates
    func initiatePeriodicalUpdate(with interval: TimeInterval = 1.0) {
        dispose = DisposeBag()
        Observable<Int>
            .interval(interval, scheduler: MainScheduler.asyncInstance)
            .subscribe(
                onNext: { [weak self] _ in
                    self?.load()
                }
            )
            .disposed(by: dispose)
    }
    
    /// Stop periodical update
    func stopPeriodicalUpdate() {
        dispose = DisposeBag()
    }
    
    private func load() {
        api.getListOfCurrencies(with: currenciesSection.baseCurrencyID) { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let currencies):
                DispatchQueue.main.async {
                    strongSelf.currenciesSection.updateRates(with: currencies)
                }
            case .error(let error):
                print(error)
            }
        }
    }
    
}

extension CurrenciesListViewModel: CurrenciesListTableViewCurrencySectionProtocol {
    
    func currenciesListTableViewCurrencySectionBaseCurrencyChanged(to id: CurrencyRate.ID) {
        delegate?.vmUpdatePrompt(with: id)
    }
    
}

