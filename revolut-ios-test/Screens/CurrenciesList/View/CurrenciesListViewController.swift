//
//  CurrenciesListViewController.swift
//  revolut-ios-test
//
//  Created by Alexander Danilyak on 30.04.2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

/// View Controller with list of exchange rates
class CurrenciesListViewController: UIViewController {
    
    private let vm: CurrenciesListViewModel
    
    let tableView: UITableView = UITableView()
    
    init(vm: CurrenciesListViewModel) {
        self.vm = vm
        super.init(nibName: nil, bundle: nil)
        
        self.vm.delegate = self
        tableViewSetup()
        layout()
    }
    
    //MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = tr("vc.currency_list.title")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        vm.initiatePeriodicalUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        vm.stopPeriodicalUpdate()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("CurrenciesListViewController cann't be initialize from interface builder")
    }
    
    //MARK: Layout
    
    private func layout() {
        view.addSubview(tableView)
        
        tableView.snp.remakeConstraints { maker in
            maker.edges.equalToSuperview()
        }
    }
    
    //MARK: Table View
    
    private func tableViewSetup(){
        tableView.delegate = vm
        tableView.dataSource = vm
        vm.interactionDelegate = self
        
        tableView.keyboardDismissMode = .onDrag
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 56.0
        
        vm.sections.forEach {
            $0.cellClassesAndReuseIdentifiers()
                .forEach { tableView.register($0.0, forCellReuseIdentifier: $0.1) }
        }
    }
    
}

extension CurrenciesListViewController: TableViewMediatorContraryInteractionProtocol, TableViewOwnerProtocol {}

extension CurrenciesListViewController: CurrenciesListViewModelProtocol {
    
    func vmUpdatePrompt(with baseID: String) {
        navigationItem.prompt = tr("vc.currency_list.prompt") + baseID
    }
    
}

