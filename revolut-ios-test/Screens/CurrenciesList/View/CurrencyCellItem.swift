//
//  CurrencyCellItem.swift
//  revolut-ios-test
//
//  Created by Alexander Danilyak on 30.04.2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation

struct CurrencyCellItem {
    
    let currencyKey: String
    
    let value: String
    
    init(currencyKey: String, value: Double) {
        self.currencyKey = currencyKey
        self.value = currencyDecimalNumberFormatter.string(from: NSNumber(value: value)) ?? "-"
    }
    
    /// Dummy item, not for use
    static let empty: CurrencyCellItem
        = CurrencyCellItem(currencyKey: "", value: 0.0)
    
}
