//
//  CurrencyCell.swift
//  revolut-ios-test
//
//  Created by Alexander Danilyak on 30.04.2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit
import SnapKit

/// Protocol for interaction with view model
protocol CurrencyCellProtocol: class {
    
    /// Input did changed
    ///
    /// - Parameters:
    ///   - input: input
    ///   - cell: edited cell
    func currencyCellInputDidChanged(to input: String, in cell: CurrencyCell)
    
}

class CurrencyCell: UITableViewCell {
    
    private let titleLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = StyleGuide.shared.textFont
        label.textColor = StyleGuide.shared.textColor
        return label
    }()
    
    private lazy var textInputField: UITextField = {
        let textField: UITextField = UITextField()
        textField.font = StyleGuide.shared.mediumTextFont
        textField.textColor = StyleGuide.shared.textColor
        textField.textAlignment = .right
        textField.keyboardType = .decimalPad
        textField.isUserInteractionEnabled = false
        textField.delegate = self
        return textField
    }()
    
    weak var delegate: CurrencyCellProtocol?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        layout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = nil
        textInputField.text = nil
        textInputField.isUserInteractionEnabled = false
    }
    
    override func becomeFirstResponder() -> Bool {
        textInputField.isUserInteractionEnabled = true
        return textInputField.becomeFirstResponder()
    }
    
    override func resignFirstResponder() -> Bool {
        textInputField.isUserInteractionEnabled = false
        return textInputField.resignFirstResponder()
    }
    
    override var isFirstResponder: Bool {
        return textInputField.isFirstResponder
    }
    
    //MARK: Layout
    
    private func layout() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(textInputField)
        
        titleLabel.snp.remakeConstraints { maker in
            maker.left.top.bottom.equalToSuperview().inset(16.0)
            //maker.width.equalTo(40.0)
        }
        
        textInputField.snp.remakeConstraints { maker in
            maker.centerY.equalToSuperview()
            maker.right.equalToSuperview().inset(16.0)
            maker.left.greaterThanOrEqualTo(titleLabel.snp.right).offset(8.0)
            maker.height.equalTo(24.0)
        }
    }
    
    //MARK: Configure
    
    func configure(with item: CurrencyCellItem) {
        titleLabel.text = item.currencyKey
        textInputField.text = item.value
    }
    
}

// MARK: - UITextFieldDelegate
extension CurrencyCell: UITextFieldDelegate {
    
    /// Default decimal delimiter according to locale
    private var decimalDelimiter: String? {
        return Locale.current.decimalSeparator
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let decimalDelimiter: String = decimalDelimiter,
            string == decimalDelimiter,
            textField.text?.range(of: decimalDelimiter) != nil {
            return false
        }
        
        if let updatedText: String = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) {
            delegate?.currencyCellInputDidChanged(to: updatedText, in: self)
        }
        
        return true
    }
    
}
