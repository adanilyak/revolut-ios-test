//
//  CurrenciesListRouter.swift
//  revolut-ios-test
//
//  Created by Alexander Danilyak on 30.04.2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

protocol CurrenciesListRouter: class {
    
    /// Open currencies list controller
    func routeToCurrenciesList()
    
}

extension Router: CurrenciesListRouter {
    
    func routeToCurrenciesList() {
        let vc: UIViewController = assembler.assembleCurrenciesListViewController(with: self)
        rootController.pushViewController(vc, animated: true)
    }
    
}
