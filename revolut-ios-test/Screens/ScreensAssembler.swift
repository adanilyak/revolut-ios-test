//
//  ScreensAssembler.swift
//  revolut-ios-test
//
//  Created by Alexander Danilyak on 30.04.2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

/// Assembler for building all possible application screens
class ScreensAssembler {
    
    //MARK: Services
    
    let apiService: APIService = APIService()
    
}

extension ScreensAssembler: ScreensAssemblerProtocol {
    
    func assembleCurrenciesListViewController(with router: CurrenciesListRouter) -> UIViewController {
        let vm: CurrenciesListViewModel = CurrenciesListViewModel(with: router, apiServiceCurrency: apiService)
        let vc: CurrenciesListViewController = CurrenciesListViewController(vm: vm)
        return vc
    }
    
}
