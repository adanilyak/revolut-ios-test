//
//  APIServiceProtocol.swift
//  revolut-ios-test
//
//  Created by Alexander Danilyak on 29.04.2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation

enum APIResult<Value> {
    case success(Value)
    case error(Error)
}

typealias APICompletion<Value> = (APIResult<Value>) -> Void

protocol APIServiceProtocol: APIServiceProtocolCurrency {}
