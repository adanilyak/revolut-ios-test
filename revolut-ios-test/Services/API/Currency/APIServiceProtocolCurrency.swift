//
//  APIServiceProtocolCurrency.swift
//  revolut-ios-test
//
//  Created by Alexander Danilyak on 30.04.2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation

protocol APIServiceProtocolCurrency: class {
    
    func getListOfCurrencies(with base: CurrencyRate.ID, completion: APICompletion<[CurrencyRate]>?)
    
}

extension APIService {
    
    func getListOfCurrencies(with base: CurrencyRate.ID, completion: APICompletion<[CurrencyRate]>?) {
        guard let url = createURL(pathComponents: ["latest"], queryParams: ["base" : "\(base)"]) else {
            completion?(APIResult.error(APIServiceError.invalidURL))
            return
        }
        
        let request: URLRequest = createRequest(with: url, method: .get)
        
        let dataTask: URLSessionDataTask = urlSession.dataTask(with: request) { (data, response, error) in
            if let error = error {
                completion?(APIResult.error(APIServiceError.response(error)))
                return
            }
            
            guard let data: Data = data else {
                completion?(APIResult.error(APIServiceError.dataExpected))
                return
            }
            
            guard let jsonObject: Any = try? JSONSerialization.jsonObject(with: data, options: []),
                let json: JSON = jsonObject as? JSON else {
                    completion?(APIResult.error(APIServiceError.serialization))
                    return
            }
            
            guard let ratesObject: JSON = json["rates"] as? JSON,
                let rates: [String: Double] = ratesObject as? [String: Double] else {
                    completion?(APIResult.error(APIServiceError.serialization))
                    return
            }
            
            let currencies: [CurrencyRate] = rates.compactMap { CurrencyRate(id: $0.key, value: $0.value, base: base) }
            completion?(APIResult.success(currencies))
        }
        
        dataTask.resume()
    }
    
}
