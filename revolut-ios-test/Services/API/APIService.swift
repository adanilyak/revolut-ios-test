//
//  APIService.swift
//  revolut-ios-test
//
//  Created by Alexander Danilyak on 29.04.2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation

class APIService {
    
    let serviceConfiguration: APIServiceConfiguration
    
    let urlSession: URLSession = {
        let configuration: URLSessionConfiguration = URLSessionConfiguration.default
        return URLSession(configuration: configuration)
    }()
    
    init(serviceConfiguration: APIServiceConfiguration = APIServiceConfiguration()) {
        self.serviceConfiguration = serviceConfiguration
    }
    
}

struct APIServiceConfiguration {
    
    let scheme: String
    
    let host: String
    
    init(scheme: String = "https", host: String = "revolut.duckdns.org") {
        self.scheme = scheme
        self.host = host
    }
    
}

enum APIServiceError: Error {
    case invalidURL
    case response(Error)
    case dataExpected
    case serialization
}

extension APIService: APIServiceProtocol {}

extension APIService {
    
    //MARK: Aliases
    
    typealias QueryParams = [String: String]
    
    typealias JSON = [String: Any]
    
    //MARK: URL Builder
    
    func createURL(pathComponents: [String], queryParams: QueryParams = [:]) -> URL? {
        var urlComponents: URLComponents = URLComponents()
        urlComponents.scheme = serviceConfiguration.scheme
        urlComponents.host = serviceConfiguration.host
        
        if !pathComponents.isEmpty {
            urlComponents.path = "/" + pathComponents.joined(separator: "/")
        }
        
        if !queryParams.isEmpty {
            urlComponents.queryItems = queryParams.map { URLQueryItem(name: $0.key, value: $0.value) }
        }
        
        return urlComponents.url
    }
    
    //MARK: Request Builder
    
    enum Method: String {
        case get = "GET"
        case post = "POST"
        case put = "PUT"
        case delete = "DELETE"
    }
    
    func createRequest(with url: URL, method: Method, params: JSON? = nil) -> URLRequest {
        var request: URLRequest = URLRequest(url: url)
        request.httpMethod = method.rawValue
        if let params: JSON = params {
            request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        }
        
        return request
    }
    
}
