//
//  TableViewMediatorContraryInteractionProtocol.swift
//  revolut-ios-test
//
//  Created by Alexander Danilyak on 03.05.2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

protocol TableViewMediatorContraryInteractionProtocol: class {
    
    func getIndexPath(of cell: UITableViewCell) -> IndexPath?
    
    func getVisibleCellsIndeciesFor(section: Int) -> IndexSet?
    
    func getCell(at indexPath: IndexPath) -> UITableViewCell?
    
    func insertCells(at indexPaths: [IndexPath])
    
    func moveCell(from oldIndexPath: IndexPath,
                  to newIndexPath: IndexPath,
                  and completion: ((Bool) -> Void)?)
    
    func deselect(at indexPath: IndexPath,
                  animated: Bool)
    
    func scroll(to indexPath: IndexPath,
                position: UITableViewScrollPosition)
    
}
