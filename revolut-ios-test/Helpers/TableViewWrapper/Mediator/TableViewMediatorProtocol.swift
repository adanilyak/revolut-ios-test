//
//  TableViewMediatorProtocol.swift
//  revolut-ios-test
//
//  Created by Alexander Danilyak on 03.05.2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

protocol TableViewMediatorProtocol: class {
    
    var sections: [TableViewSectionProtocol] { get }
    
}

@objc class AbstactTableViewMediator: NSObject, TableViewMediatorProtocol {
    
    var sections: [TableViewSectionProtocol] {
        fatalError("Should be overriden in subclass")
    }
    
    weak var interactionDelegate: TableViewMediatorContraryInteractionProtocol?
    
}

extension AbstactTableViewMediator: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row: Int = indexPath.row
        let section: Int = indexPath.section
        
        let reuseIdentifier: String = sections[section].cellReuseIdentifier(for: row)
        let dequeuedCell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
        return sections[section].cell(for: row, dequeuedCell: dequeuedCell)
    }
    
}

extension AbstactTableViewMediator: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        sections[indexPath.section].onSelect(at: indexPath.row)
    }
    
}

extension AbstactTableViewMediator: TableViewSectionContraryInteractionProtocol {
    
    func getIndex(for section: TableViewSectionProtocol) -> Int? {
        return sections.index(where: { isSameSections(lhs: $0, rhs: section) })
    }
    
    func getIndex(of cell: UITableViewCell, for section: TableViewSectionProtocol) -> Int? {
        return interactionDelegate?.getIndexPath(of: cell)?.row
    }
    
    func getVisibleCellsIndecies(for section: TableViewSectionProtocol) -> IndexSet? {
        guard let index: Int = getIndex(for: section) else { return nil }
        return interactionDelegate?.getVisibleCellsIndeciesFor(section: index)
    }
    
    func getCell(at index: Int, for section: TableViewSectionProtocol) -> UITableViewCell? {
        guard let sectionIndex: Int = getIndex(for: section) else { return nil }
        return interactionDelegate?.getCell(at: IndexPath(row: index, section: sectionIndex))
    }
    
    func insertCells(at indecies: IndexSet, for section: TableViewSectionProtocol) {
        guard let index: Int = getIndex(for: section) else { return }
        interactionDelegate?.insertCells(at: indecies.map { IndexPath(row: $0, section: index) })
    }
    
    func moveCell(from oldIndex: Int, to newIndex: Int, and completion: ((Bool) -> Void)?, for section: TableViewSectionProtocol) {
        guard let index: Int = getIndex(for: section) else { return }
        interactionDelegate?.moveCell(from: IndexPath(row: oldIndex, section: index),
                                      to: IndexPath(row: newIndex, section: index),
                                      and: completion)
    }
    
    func deselect(at index: Int, animated: Bool, for section: TableViewSectionProtocol) {
        guard let sectionIndex: Int = getIndex(for: section) else { return }
        interactionDelegate?.deselect(at: IndexPath(row: index, section: sectionIndex), animated: animated)
    }
    
    func scroll(to index: Int, position: UITableViewScrollPosition, for section: TableViewSectionProtocol) {
        guard let sectionIndex: Int = getIndex(for: section) else { return }
        interactionDelegate?.scroll(to: IndexPath(row: index, section: sectionIndex), position: position)
    }
    
}
