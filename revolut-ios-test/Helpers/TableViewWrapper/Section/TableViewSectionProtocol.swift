//
//  TableViewSectionProtocol.swift
//  revolut-ios-test
//
//  Created by Alexander Danilyak on 03.05.2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

protocol TableViewSectionProtocol: class {
    
    var interactionDelegate: TableViewSectionContraryInteractionProtocol? { get set }
    
    func cellClassesAndReuseIdentifiers() -> [(UITableViewCell.Type, String)]
    
    func numberOfRows() -> Int
    
    func cellReuseIdentifier(for row: Int) -> String
    
    func cell(for row: Int, dequeuedCell: UITableViewCell) -> UITableViewCell
    
    func onSelect(at index: Int)
    
}

func isSameSections(lhs: TableViewSectionProtocol, rhs: TableViewSectionProtocol) -> Bool {
    return ObjectIdentifier(lhs) == ObjectIdentifier(rhs)
}
