//
//  TableViewSectionContraryInteractionProtocol.swift
//  revolut-ios-test
//
//  Created by Alexander Danilyak on 03.05.2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

protocol TableViewSectionContraryInteractionProtocol: class {
    
    func getIndex(of cell: UITableViewCell,
                  for section: TableViewSectionProtocol) -> Int?
    
    func getVisibleCellsIndecies(for section: TableViewSectionProtocol) -> IndexSet?
    
    func getCell(at index: Int,
                 for section: TableViewSectionProtocol) -> UITableViewCell?
    
    func insertCells(at indecies: IndexSet,
                     for section: TableViewSectionProtocol)
    
    func moveCell(from oldIndex: Int,
                  to newIndex: Int,
                  and completion: ((Bool) -> Void)?,
                  for section: TableViewSectionProtocol)
    
    func deselect(at index: Int,
                  animated: Bool,
                  for section: TableViewSectionProtocol)
    
    func scroll(to index: Int,
                position: UITableViewScrollPosition,
                for section: TableViewSectionProtocol)
    
}
