//
//  TableViewOwnerProtocol.swift
//  revolut-ios-test
//
//  Created by Alexander Danilyak on 03.05.2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

protocol TableViewOwnerProtocol: class {
    
    var tableView: UITableView { get }
    
}

extension TableViewMediatorContraryInteractionProtocol where Self: TableViewOwnerProtocol {
    
    func getIndexPath(of cell: UITableViewCell) -> IndexPath? {
        return tableView.indexPath(for: cell)
    }
    
    func getVisibleCellsIndeciesFor(section: Int) -> IndexSet? {
        guard let indexPaths: [IndexPath] = tableView.indexPathsForVisibleRows else {
            return nil
        }
        
        var visibleIndecies: IndexSet = IndexSet()
        indexPaths
            .filter { $0.section == section }
            .forEach { visibleIndecies.insert($0.row) }
        
        return visibleIndecies
    }
    
    func getCell(at indexPath: IndexPath) -> UITableViewCell? {
        return tableView.cellForRow(at: indexPath)
    }
    
    func insertCells(at indexPaths: [IndexPath]) {
        tableView.insertRows(at: indexPaths, with: .automatic)
    }
    
    func moveCell(from oldIndexPath: IndexPath,
                  to newIndexPath: IndexPath,
                  and completion: ((Bool) -> Void)?) {
        tableView.performBatchUpdates({ [weak self] in self?.tableView.moveRow(at: oldIndexPath, to: newIndexPath) },
                                      completion: completion)
    }
    
    func deselect(at indexPath: IndexPath,
                  animated: Bool) {
        tableView.deselectRow(at: indexPath,
                              animated: animated)
    }
    
    func scroll(to indexPath: IndexPath,
                position: UITableViewScrollPosition) {
        tableView.scrollToRow(at: indexPath,
                              at: position,
                              animated: true)
    }
    
}
