//
//  StyleGuide.swift
//  revolut-ios-test
//
//  Created by Alexander Danilyak on 30.04.2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import UIKit

/// Common app style
class StyleGuide {
    
    /// Singleton
    static let shared: StyleGuide = StyleGuide()
    
    private init() {}
    
    //MARK: Colors
    
    let textColor: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    
    let backgroundColor: UIColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    
    //MARK: Font
    
    let textFont: UIFont = UIFont.systemFont(ofSize: 16.0)
    
    let mediumTextFont: UIFont = UIFont.systemFont(ofSize: 16.0, weight: .medium)
    
}
