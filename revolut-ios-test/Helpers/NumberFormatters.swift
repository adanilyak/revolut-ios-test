//
//  NumberFormatters.swift
//  revolut-ios-test
//
//  Created by Alexander Danilyak on 03.05.2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation

let currencyDecimalNumberFormatter: NumberFormatter = {
    let formatter: NumberFormatter = NumberFormatter()
    formatter.decimalSeparator = Locale.current.decimalSeparator
    formatter.usesGroupingSeparator = false
    formatter.numberStyle = .decimal
    return formatter
}()
