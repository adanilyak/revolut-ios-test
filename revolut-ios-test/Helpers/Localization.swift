//
//  Localization.swift
//  revolut-ios-test
//
//  Created by Alexander Danilyak on 30.04.2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation

/// Get localized string by key
///
/// - Parameter key: key
/// - Returns: localized string
func tr(_ key: String) -> String {
    return NSLocalizedString(key, comment: "")
}
