//
//  Currency.swift
//  revolut-ios-test
//
//  Created by Alexander Danilyak on 29.04.2018.
//  Copyright © 2018 Alexander Danilyak. All rights reserved.
//

import Foundation

/// Exchange rate
struct CurrencyRate {
    
    /// Alias for currency ID string
    typealias ID = String
    
    /// ID
    let id: ID
    
    /// Exchange rate between id and base
    let value: Double
    
    /// Base currency ID
    let base: ID
    
}
